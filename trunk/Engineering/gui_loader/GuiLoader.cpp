#include <pugixml/pugixml.hpp>

#include "GuiLoader.h"

/// GUILoader class implementation

bool GUILoader::Load(std::string sPath, GuiSM::Widgets::WidgetPtr pParent)
{
	pugi::xml_document doc;
	doc.load_file(sPath.c_str());
	pugi::xml_node root = doc.child("vguismd");
	if(!root || !pParent)
		return false;

	GuiSM::Widgets::WidgetPtr pWidget;

	pugi::xml_node widget = root.child("widget");
	while(widget && pParent)
	{
		WidgetData wd;
		wd.name			=	std::string( widget.attribute("name").value() );
		wd.type			=	GuiSM::Widgets::WidgetType( widget.attribute("type").as_int() );
		wd.x			=	widget.child("position").attribute("x").as_int();
		wd.y			=	widget.child("position").attribute("y").as_int();
		wd.w			= 	widget.child("size").attribute("w").as_int();
		wd.h			=	widget.child("size").attribute("h").as_int();
		wd.texturePath	=	std::string( widget.attribute("texture_path").value() );
		
		pWidget = AddWidget(wd, pParent);

		if(widget.child("widget"))
		{
			pParent = pWidget;
			widget = widget.child("widget");
		}
		else if(widget.next_sibling())
		{
			widget = widget.next_sibling();
		}
		else
		{			
			pParent = pWidget->getParent()->getParent();
			widget = widget.parent().next_sibling();
		}		
	}
	return true;
}

GuiSM::Widgets::WidgetPtr GUILoader::AddWidget(WidgetData& widget, GuiSM::Widgets::WidgetPtr pParent)
{
	GuiSM::Widgets::WidgetPtr pWidget;
	GuiSM::Rect r;

	r.x = widget.x;
	r.y = widget.y;
	r.width = widget.w;
	r.height = widget.h;						

	switch(widget.type)
	{
						
	case GuiSM::Widgets::WidgetType::EWT_BUTTON:
		pWidget = GuiSM::Widgets::CSimpleButton::Create(r, pParent );
		break;
			
	case GuiSM::Widgets::WidgetType::EWT_CHECKBOX:
		pWidget = GuiSM::Widgets::CSimpleCheckBox::Create(pParent);
		pWidget->setSize(r);
		break;

	case GuiSM::Widgets::WidgetType::EWT_EDIT:
		pWidget = GuiSM::Widgets::CSimpleEdit::Create(pParent);
		pWidget->setSize(r);
		break;

	case GuiSM::Widgets::WidgetType::EWT_IMAGE:
		if(!widget.texturePath.empty())
			pWidget = GuiSM::Widgets::CSimpleImage::Create(r, widget.texturePath, pParent);
		else
			pWidget = GuiSM::Widgets::CSimpleImage::Create(r, pParent);
		break;

	case GuiSM::Widgets::WidgetType::EWT_LAYER:
		//pWidget = GuiSM::Widgets::CSimpleLayer::Create(r, pParent );
		GuiSM::GUIManager::get().addLayer(widget.name);
		break;

	case GuiSM::Widgets::WidgetType::EWT_PROGRESSBAR:
		pWidget = GuiSM::Widgets::CSimpleProgressBar::Create(r, pParent);
		break;

	case GuiSM::Widgets::WidgetType::EWT_RADIOBUTTON:
		pWidget = GuiSM::Widgets::CSimpleRadioButton::Create(pParent);
		pWidget->setSize(r);
		break;

	case GuiSM::Widgets::WidgetType::EWT_SCROLL:
		pWidget = GuiSM::Widgets::CSimpleScroll::Create(r, pParent);
		break;

	case GuiSM::Widgets::WidgetType::EWT_TEXTBLOCK:
		//pWidget = GuiSM::Widgets::CSimpleProgressBar::Create(r, pParent);
		break;

	case GuiSM::Widgets::WidgetType::EWT_WINDOW:
		pWidget = GuiSM::Widgets::CSimpleWindow::Create(r, pParent );
		break;

	case GuiSM::Widgets::WidgetType::EWT_NONE:
		break;
					
	}
	if(pWidget)
	{
		if(!widget.texturePath.empty() && widget.type != GuiSM::Widgets::WidgetType::EWT_IMAGE)
			pWidget->setTexture(*GuiSM::TextureManager::get().getTexture(widget.texturePath));
		if(!widget.name.empty())
			pWidget->setName(widget.name);
	}
	return pWidget;
}

