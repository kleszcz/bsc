#pragma once

#include <string>
#include <vector>

#include <GuiSM/GUIManager.h>

// Any new widget should be included here
#include <GuiSM/Window.h>
#include <GuiSM/Button.h>
#include <GuiSM/Layer.h>
#include <GuiSM/Image.h>
#include <GuiSM/Progressbar.h>
#include <GuiSM/Scrollbar.h>
#include <GuiSM/Widget.h>
//#include <GuiSM/CheckBox.h>
/////////////////////////////////////////////

/// Widget definition structure

/// \author Jan Kleszczyński
struct WidgetData
{	
	/// Defoult constructor.
	WidgetData() : 
		type(GuiSM::Widgets::WidgetType::EWT_NONE),
		x(0),
		y(0),
		w(0),
		h(0)
	{
		texturePath.clear();
		parentName.clear();
		name.clear();
	}

	/// Type of widget. Same as widget template parameter.
	GuiSM::Widgets::WidgetType type;
	/// Postion x
	int x;
	/// Position y
	int y;
	/// Width
	int w;
	/// Height
	int h;
	/// Path to texture file
	std::string texturePath;
	/// Name of parent widget
	std::string parentName;
	/// Name of widget
	std::string name;
};



/// Loads and runs gui from xml.

/// \author Jan Kleszczyński
class GUILoader
{
public:
	/// Loads widgets from xml. Set  as written in xml file.
	/// \param sPath std::string xml document path
	/// \param pParent GuiSM::Widgets::WidgetPtr parent for gui component (usually a layer)
	/// \retval bool true if succeeded
	static bool Load(std::string sPath, GuiSM::Widgets::WidgetPtr pParent);
	
private:
	/// Creats widget and adds them to GUIManager setting size, texture, parent and name.
	/// \param WidgetData widget definition to add to the list
	/// \param pParent GuiSM::Widgets::WidgetPtr widget parent
	static GuiSM::Widgets::WidgetPtr AddWidget(WidgetData& widget, GuiSM::Widgets::WidgetPtr pRoot);

};
