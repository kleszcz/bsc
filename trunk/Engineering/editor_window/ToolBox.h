#pragma once

#include <GuiSM/GUIManager.h>
#include <GuiLoader.h>
#include <GuiSM\FastDelegate\FastDelegate.h>
#include <GuiSM\FastDelegate\FastDelegateBind.h>
#include <vector>
#include <string>
#include "GUICreatorMgr.h"


class ToolBox
{
public:
	ToolBox(void);
	~ToolBox(void);

	void AddWindowButtonOnMouseClick(GuiSM::Events::MouseEvent* e);
	void AddButtonButtonOnMouseClick(GuiSM::Events::MouseEvent* e);
	void SaveButton(GuiSM::Events::MouseEvent* e);
	void Add(GuiSM::Events::MouseEvent* e);

	int windowsNum;
	int buttonsNum;

};

