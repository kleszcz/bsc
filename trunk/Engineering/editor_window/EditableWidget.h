#pragma once

#include <GuiSM/GUIManager.h>
#include <GuiLoader.h>
#include <GuiSM\FastDelegate\FastDelegate.h>
#include <GuiSM\FastDelegate\FastDelegateBind.h>
#include <vector>
#include <GuiSM\Window.h>
#include <pugixml\pugixml.hpp>


class EditableWidget
{
public:
	EditableWidget(GuiSM::Rect r, GuiSM::Widgets::WidgetPtr parent);
	~EditableWidget(void);

	/////
	
	void Add(GuiSM::Events::MouseEvent* e);
	void MouseEnter(GuiSM::Events::MouseEvent* e);
	void MoveButtonUp(GuiSM::Events::MouseButtonEvent* e);
	void MoveButtonDown(GuiSM::Events::MouseButtonEvent* e);
	void MoveWidget(GuiSM::Events::MouseMoveEvent* e);
	void ResizeButtonUp(GuiSM::Events::MouseButtonEvent* e);
	void ResizeButtonDown(GuiSM::Events::MouseButtonEvent* e);
	void ResizeWidget(GuiSM::Events::MouseMoveEvent* e);
	void MouseLeave(GuiSM::Events::MouseEvent* e);
	/////

	GuiSM::Widgets::WidgetPtr _self;
	GuiSM::Widgets::WidgetPtr _resize;
	GuiSM::Widgets::WidgetPtr _move;

	void ToXml(pugi::xml_node root);

	bool _moving;
	bool _resizing;

	int _handleSize;

	std::vector<EditableWidget*> _children;
	WidgetData data;
};

