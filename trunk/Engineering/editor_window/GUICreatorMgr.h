#pragma once

#include <vector>
#include <string>
#include <GuiLoader.h>
#include <pugixml\pugixml.hpp>

#include "EditableWidget.h"




class GUICreatorMgr
{
public:
	static  GUICreatorMgr* GetInstance();
	~GUICreatorMgr(void);

	void Save(std::string name);
	

public:
	bool Adding;
	std::string Name;
	GuiSM::Widgets::WidgetType Type;
	GuiSM::Rect Size;
	std::vector<EditableWidget*> Widgets;

private:
	GUICreatorMgr(void);

private:
	static GUICreatorMgr* _self;
};

