#include "ToolBox.h"


using namespace GuiSM;
using namespace GuiSM::Widgets;



#include <cstdio>




ToolBox::ToolBox(void)
{
	WidgetPtr layer = GuiSM::GUIManager::get().getActiveLayer();
	GUILoader::Load("data/gui/toolbox.xml", layer);

	fastdelegate::FastDelegate< void(Events::MouseEvent* ) > delegate;
	delegate.bind(this, &ToolBox::AddWindowButtonOnMouseClick);	
	WidgetPtr pWidget = layer->getChild("ToolBoxWindow")->getChild("AddWindowButton");
	pWidget->attach( delegate, Events::EET_MOUSECLICK);
	boost::shared_static_cast<GuiSM::Widgets::CSimpleButton>(pWidget)->setCaption("Add Window");

	delegate.bind(this, &ToolBox::AddButtonButtonOnMouseClick);	
	pWidget = layer->getChild("ToolBoxWindow")->getChild("AddButtonButton");
	pWidget->attach( delegate, Events::EET_MOUSECLICK);
	boost::shared_static_cast<GuiSM::Widgets::CSimpleButton>(pWidget)->setCaption("Add Button");

	delegate.bind(this, &ToolBox::SaveButton);	
	pWidget = layer->getChild("ToolBoxWindow")->getChild("SaveButton");
	pWidget->attach( delegate, Events::EET_MOUSECLICK);
	boost::shared_static_cast<GuiSM::Widgets::CSimpleButton>(pWidget)->setCaption("Save");

	fastdelegate::FastDelegate< void(Events::MouseEvent* ) > delegate2;
	delegate2.bind(this, &ToolBox::Add);	
	
	layer->attach( delegate2, Events::EET_MOUSECLICK);
	windowsNum = 0;
	buttonsNum = 0;

}

void ToolBox::AddWindowButtonOnMouseClick(GuiSM::Events::MouseEvent* e)
{	
	GUICreatorMgr::GetInstance()->Adding = true;
	char name[64];
	++windowsNum;
	std::sprintf(name, "GuiWindow%02d", windowsNum);
	GUICreatorMgr::GetInstance()->Name = std::string(name);
	GUICreatorMgr::GetInstance()->Type = GuiSM::Widgets::EWT_WINDOW;
	GUICreatorMgr::GetInstance()->Size.height = 200;
	GUICreatorMgr::GetInstance()->Size.width = 200;
}

void ToolBox::AddButtonButtonOnMouseClick(GuiSM::Events::MouseEvent* e)
{
	GUICreatorMgr::GetInstance()->Adding = true;
	char name[64];
	++buttonsNum;
	std::sprintf(name, "GuiButton%02d", buttonsNum);
	GUICreatorMgr::GetInstance()->Name = std::string(name);
	GUICreatorMgr::GetInstance()->Type = GuiSM::Widgets::EWT_BUTTON;
	GUICreatorMgr::GetInstance()->Size.height = 25;
	GUICreatorMgr::GetInstance()->Size.width = 100;
}

void ToolBox::SaveButton(GuiSM::Events::MouseEvent* e)
{
	GUICreatorMgr::GetInstance()->Save("new.xml");
}

ToolBox::~ToolBox(void)
{
}

void ToolBox::Add(GuiSM::Events::MouseEvent* e)
{
	if(GUICreatorMgr::GetInstance()->Adding)
	{
		WidgetPtr layer = GuiSM::GUIManager::get().getActiveLayer();
		GuiSM::Rect& r = GUICreatorMgr::GetInstance()->Size;
		r.x = e->position.x;
		r.y = e->position.y;
		GUICreatorMgr::GetInstance()->Widgets.push_back(new EditableWidget(r, layer));
	}
	GUICreatorMgr::GetInstance()->Adding = false;
}

