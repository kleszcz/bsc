#include "EditableWidget.h"
#include "GUICreatorMgr.h"
using namespace GuiSM;
using namespace GuiSM::Widgets;


//TODO Przerobi� to tak rzeby przyjmowa�o jako parametr typ widgeta i takowy dodawa�o bo b�dzie lepiej
EditableWidget::EditableWidget(GuiSM::Rect r, GuiSM::Widgets::WidgetPtr parent) : _handleSize(20)
{
	_self = CSimpleWindow::Create(r , parent);
	
	
	
	_move = CSimpleButton::Create(Rect(0, 0, _handleSize, _handleSize), _self);
	int y = _self->getSize().height;
	int x = _self->getSize().width;
	_resize = CSimpleButton::Create(Rect(x-_handleSize, y-_handleSize, _handleSize, _handleSize), _self);

	_moving = false;
	_resizing = false;

	_resize	->setVisibility(false);
	_move	->setVisibility(false);

	_resize->setTexture(*GuiSM::TextureManager::get().getTexture("data/gfx/resize.tga"));
	_move->setTexture(*GuiSM::TextureManager::get().getTexture("data/gfx/move.tga"));

	//TODO Do totalnej pzrer�bki, r�wnie� je�eli chodzi o to jak jest parametr przekazywany...
	if(GUICreatorMgr::GetInstance()->Type == GuiSM::Widgets::EWT_BUTTON)
		_self->setTexture(*GuiSM::TextureManager::get().getTexture("data/gfx/addedButton.tga"));

	data.x = r.x;
	data.y = r.y;
	data.h = r.height;
	data.w = r.width;
	data.parentName = parent->getName();
	data.name = GUICreatorMgr::GetInstance()->Name;
	data.type = GUICreatorMgr::GetInstance()->Type;
	fastdelegate::FastDelegate< void(Events::MouseEvent* ) > delegate;
	fastdelegate::FastDelegate< void(Events::MouseButtonEvent* ) > delegateButton;
	fastdelegate::FastDelegate< void(Events::MouseMoveEvent* ) > delegateMove;

	delegate.bind(this, &EditableWidget::Add);		
	_self->attach( delegate, Events::EET_MOUSECLICK);

	delegate.bind(this, &EditableWidget::MouseEnter);
//	_self	->attach( delegate, Events::EET_MOUSEENTER);
	_resize	->attach( delegate, Events::EET_MOUSEOVER);
	_move	->attach( delegate, Events::EET_MOUSEOVER);

	delegate.bind(this, &EditableWidget::MouseLeave);
//	_self	->attach( delegate, Events::EET_MOUSELEAVE);
	_resize	->attach( delegate, Events::EET_MOUSELEAVE);
	_move	->attach( delegate, Events::EET_MOUSELEAVE);

	
	delegateButton.bind(this, &EditableWidget::MoveButtonDown);
	_move	->attach( delegateButton, Events::EET_MOUSEBUTTONDOWN);
	delegateButton.bind(this, &EditableWidget::MoveButtonUp);
	_move	->attach( delegateButton, Events::EET_MOUSEBUTTONUP);

	
	delegateMove.bind(this, &EditableWidget::MoveWidget);
	_move	->attach( delegateMove, Events::EET_MOUSEMOVE);

	delegateButton.bind(this, &EditableWidget::ResizeButtonDown);
	_resize	->attach( delegateButton, Events::EET_MOUSEBUTTONDOWN);
	delegateButton.bind(this, &EditableWidget::ResizeButtonUp);
	_resize	->attach( delegateButton, Events::EET_MOUSEBUTTONUP);

	delegateMove.bind(this, &EditableWidget::ResizeWidget);
	_resize	->attach( delegateMove, Events::EET_MOUSEMOVE);

}


EditableWidget::~EditableWidget(void)
{
}

void EditableWidget::MoveButtonDown(GuiSM::Events::MouseButtonEvent* e)
{
	if(e->button == EMOUSEBUTTON::EMB_LMB)
		_moving = true;
}

void EditableWidget::MoveButtonUp(GuiSM::Events::MouseButtonEvent* e)
{
	_moving = false;
}


void EditableWidget::MoveWidget(GuiSM::Events::MouseMoveEvent* e)
{
	if(_moving)
	{
		Point pos = _self->getPos() +e->current -e->previous;
		_self->setPos( pos);
		data.x = pos.x;
		data.y = pos.y;
	}
}

void EditableWidget::MouseEnter(GuiSM::Events::MouseEvent* e)
{
	_resize	->setVisibility(true);
	_move	->setVisibility(true);	
}

void EditableWidget::MouseLeave(GuiSM::Events::MouseEvent* e)
{
	_resize	->setVisibility(false);
	_move	->setVisibility(false);	
}

void EditableWidget::ResizeButtonUp(GuiSM::Events::MouseButtonEvent* e)
{
	_resizing = false;
}

void EditableWidget::ResizeButtonDown(GuiSM::Events::MouseButtonEvent* e)
{
	if(e->button == EMOUSEBUTTON::EMB_LMB)
		_resizing = true;
}

void EditableWidget::ResizeWidget(GuiSM::Events::MouseMoveEvent* e)
{
	if(_resizing)
	{
		Rect size = _self->getSize();
		Point diff =  e->current -e->previous;

		size.height += diff.y;
		size.width += diff.x;

		_self->setSize(size);

		int y = _self->getSize().height;
		int x = _self->getSize().width;
		_resize->setPos(Point(x-_handleSize, y-_handleSize));

		data.h = _self->getSize().height;
		data.w = _self->getSize().width;
	}
}

void EditableWidget::Add(GuiSM::Events::MouseEvent* e)
{
	if(GUICreatorMgr::GetInstance()->Adding)
	{
		GuiSM::Rect &r = GUICreatorMgr::GetInstance()->Size;
		r.x = e->position.x - _self->getPosReal().x;
		r.y = e->position.y - _self->getPosReal().y;
		_children.push_back(new EditableWidget(r, _self));
	}
	GUICreatorMgr::GetInstance()->Adding = false;
}

void EditableWidget::ToXml(pugi::xml_node root)
{
	pugi::xml_node widget = root.append_child("widget");
	widget.append_attribute("name").set_value(data.name.c_str());
	widget.append_attribute("type").set_value(unsigned int(data.type));
	widget.append_attribute("texture_path").set_value(data.texturePath.c_str());
	pugi::xml_node position = widget.append_child("position");
	position.append_attribute("x").set_value(data.x);
	position.append_attribute("y").set_value(data.y);
	pugi::xml_node size = widget.append_child("size");
	size.append_attribute("w").set_value(data.w);
	size.append_attribute("h").set_value(data.h);
	pugi::xml_node font_size = widget.append_child("font_size");
	font_size.append_attribute("w");
	font_size.append_attribute("h");

	int csize = _children.size();
	for(int i = 0; i < csize; ++i)
		_children[i]->ToXml(widget);
}