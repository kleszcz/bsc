#include <iostream>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <GuiSM/GUIManager.h>
#include <GuiLoader.h>

#include "ToolBox.h"

using namespace GuiSM;

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode (GL_MODELVIEW);

	GUIManager::get().render();

	glFlush();
    glutSwapBuffers();
}

void animate()
{
	glutPostRedisplay();
}

void resize(int w, int h) 
{
	if(h == 0)
		h = 1;
	float ratio = 1.0* w / h;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	gluPerspective(45,ratio,1,1000);
	glMatrixMode(GL_MODELVIEW);

	GUIManager::get().setSize( GuiSM::Point(w,h));
}

void mouse(int button, int state, int x, int y)
{
    EMOUSEBUTTON b = EMB_LMB;

    switch(button)
    {
        case GLUT_LEFT_BUTTON: b = EMB_LMB; break;
        case GLUT_MIDDLE_BUTTON: b = EMB_MMB; break;
        case GLUT_RIGHT_BUTTON: b = EMB_RMB; break;
    }

    if(state == GLUT_DOWN)
    {
        GUIManager::get().mousePress(b,x,y);
    } else
    {
        GUIManager::get().mouseRelease(b,x,y);
		GUIManager::get().mouseClick(b,x,y);
    }
}

void mouseMotion(int x,int y)
{
	GUIManager::get().mouseMove(x,y);
}

int main(int argc, char* argv[])
{

	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow(argv[0]);

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutIdleFunc(animate);
	glutMouseFunc(mouse);
	glutPassiveMotionFunc(mouseMotion);
	glutMotionFunc(mouseMotion);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GuiSM::GUIManager::get().addLayer("TEST");
	GuiSM::GUIManager::get().setActiveLayer("TEST");
	
	//ToolBox guiToolBox;

	GUILoader::Load("test.xml", GUIManager::get().getActiveLayer());
	

	glutMainLoop();
	
	
	return 0;
}