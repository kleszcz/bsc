#include "GUICreatorMgr.h"


GUICreatorMgr* GUICreatorMgr::_self = NULL;


GUICreatorMgr* GUICreatorMgr::GetInstance()
{
	if(!GUICreatorMgr::_self)
		GUICreatorMgr::_self = new GUICreatorMgr();
	return GUICreatorMgr::_self;
}


GUICreatorMgr::GUICreatorMgr(void)
{
	Adding = false;
	Widgets.clear();
}


GUICreatorMgr::~GUICreatorMgr(void)
{
}

void GUICreatorMgr::Save(std::string name)
{
	pugi::xml_document doc;
	

	
	pugi::xml_node root;
	root = doc.append_child("vguismd");
	
	root.append_attribute("version");
	root.append_attribute("date");
	root.attribute("version").set_value("0.1");
	root.attribute("date").set_value("10.06.2012");

	int size = Widgets.size();

	
	for(int i = 0; i < size; ++i)
		Widgets[i]->ToXml(root);	
	doc.save_file(name.c_str());
}