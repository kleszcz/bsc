#include "GuiEditorModel.h"

#include <GuiSM/FastDelegate/FastDelegate.h>
#include <GuiSM/FastDelegate/FastDelegateBind.h>

#include <qmessagebox.h>

#include "Exceptions.h"
#include "Utility.h"

GuiEditorModel::GuiEditorModel(GLWidget* glWidget) : pGlWidget(glWidget),
	state(MS_SELECT)
{
	activePath = QString("");

	EditableWidget::model = this;
	GUILoader::model = this;

	GuiSM::GUIManager& mgr = GuiSM::GUIManager::get();
	GuiSM::Widgets::WidgetPtr layer = mgr.addLayer("root");
	mgr.setActiveLayer("root");

	fastdelegate::FastDelegate< void(GuiSM::Events::MouseEvent* ) > delegate;
	delegate.bind(this, &GuiEditorModel::OnLayerClick);		
	layer->attach( delegate, GuiSM::Events::EET_MOUSECLICK);

	for(int i = 0; i < MS_MAX; ++i)
		widgetNum[i] = 0;

}

GuiEditorModel::~GuiEditorModel(void)
{
	
}


/** Properties slots implementation */

void GuiEditorModel::OnPropName(QString name)
{
	if(activeWidget)
		activeWidget->SetName(name);
}

void GuiEditorModel::OnPropPosX(int x)
{
	if(activeWidget)
		activeWidget->SetPosX(x);
}

void GuiEditorModel::OnPropPosY(int y)
{
	if(activeWidget)
		activeWidget->SetPosY(y);
}

void GuiEditorModel::OnPropWidth(int width)
{
	if(activeWidget)
		activeWidget->SetWidth(width);
}

void GuiEditorModel::OnPropHeight(int height)
{
	if(activeWidget)
		activeWidget->SetHeight(height);
}

void GuiEditorModel::OnPropTexPathButton()
{	
	QString path = QFileDialog::getOpenFileName(pGlWidget, tr("Open Texture"), activePath, tr("All files (*.*);;png (*.png);;tga (*.tga);;jpg( *.jpg)"));
	if(path == "")
		return;
	emit PropTexPath(path);
	OnPropTexPath(path);
}
 
void GuiEditorModel::OnPropTexPath(QString path)
{
	if(activeWidget)
		activeWidget->SetTexture(path);
}

void GuiEditorModel::OnPropDeleteButton()
{
	if(!activeWidget)
		return;
	
        QMessageBox warning(QMessageBox::Warning, "Delete widget", "It will delete selected widget and all its children. Do You want to continue?", QMessageBox::No | QMessageBox::Yes );
	QMessageBox::StandardButton result = (QMessageBox::StandardButton)warning.exec();
        if(result == QMessageBox::Yes)
	{
		activeWidget->Delete();
		ActivateWidget(EditableWidgetPtr());
	}
}

void GuiEditorModel::OnPropResizeChildren(int state)
{
	if(activeWidget)
	{
		activeWidget->SetDeepResizeChildren(state);
	}
}

/** Options slots implementation */

void GuiEditorModel::OnOptActivePath(QString path)
{
	activePath = path;
	emit OptActivePath(path);
}

void GuiEditorModel::OnOptActivePathButton()
{
	QString path = QFileDialog::getExistingDirectory(pGlWidget, tr("Set active directory"), activePath, QFileDialog::ShowDirsOnly);
	if(path == "")
		return;
	emit OptActivePath(path);
	activePath = path;
}

void GuiEditorModel::OnOptFileName(QString name)
{
	filename = name;
	emit OptFileName(name);
}

/** Actions slots implementation */
void GuiEditorModel::OnActAddWindow()
{
	state = MS_ADD_WINDOW;
}

void GuiEditorModel::OnActAddButton()
{
	state = MS_ADD_BUTTON;
}

void GuiEditorModel::OnActAddProgressbar()
{
	state = MS_ADD_PROGRESSBAR;
}

void GuiEditorModel::OnActAddImage()
{
	state = MS_ADD_IMAGE;
}

void GuiEditorModel::OnActAddScrollbar()
{
	state = MS_ADD_SCROLLBAR;
}

void GuiEditorModel::OnActAddCustomWidget()
{
	
	QString path;
	path = QFileDialog::getOpenFileName(pGlWidget, tr("Load file"), activePath, tr( "Xml (*.xml)" ) );
	if(path == "")
		return;
	state = MS_ADD_CUSTOM;
	customWidgetPath = path.toStdString();

}

void GuiEditorModel::OnActSave()
{	
	while(activePath.isEmpty() || filename.isEmpty())
	{
		if(!OpenNewFileDialog())
		{
                        QMessageBox error(QMessageBox::Critical, "Saving error", "Your file was not saved." );
			error.exec();
			return;
		}
	}
	pugi::xml_document doc;	
	pugi::xml_node root;
	root = doc.append_child("vguismd");
	
	root.append_attribute("version");
	root.append_attribute("date");
	root.attribute("version").set_value("1.0");
	
	std::string date;
	date = QDate::currentDate().toString(tr("dd.MM.yyyy")).toStdString();
	root.attribute("date").set_value(date.c_str());

	int size = widgets.size();

	
	for(int i = 0; i < size; ++i)
		widgets[i]->ToXml(root);

	std::string path;
	path = activePath.toStdString() + '/' + filename.toStdString() + ".xml";
	bool success = doc.save_file(path.c_str());

}

void GuiEditorModel::OnActLoad()
{
	if(filename != "")
	{
                QMessageBox warning(QMessageBox::Warning, "Load file", "It will delete all unsaved changes. Do You want to continue?", QMessageBox::No | QMessageBox::Yes );
		QMessageBox::StandardButton result = (QMessageBox::StandardButton)warning.exec();
                if(result == QMessageBox::No)
		{
			return;
		}
	}
	OnClear();
	QString file = QFileDialog::getOpenFileName(pGlWidget, tr("Load file"), activePath, tr( "Xml (*.xml)" ) );
	if(file == "")
		return;
	try
	{
		GUILoader::Load(file.toStdString());
	}
	catch(IncorectFileFormatException e)
	{
                QMessageBox error(QMessageBox::Critical, "Incorect file format", "Can not load selected file because the file format is invalid." );
		error.exec();
	}
	QString path = Utility::getPath(file);
	QString filename = Utility::getFileName(file);
	emit OptFileName(filename);
	this->filename = filename;
	emit OptActivePath(path);
	activePath = path;
}

void GuiEditorModel::OnActNew()
{
	if(filename != "")
	{
                QMessageBox warning(QMessageBox::Warning, "Load file", "It will delete all unsaved changes. Do You want to continue?", QMessageBox::No | QMessageBox::Yes );
		QMessageBox::StandardButton result = (QMessageBox::StandardButton)warning.exec();
                if(result == QMessageBox::No)
		{
			return;
		}
	}
	filename = "";
	while(activePath.isEmpty() || filename.isEmpty())
	{
		if(!OpenNewFileDialog())
		{
                        QMessageBox error(QMessageBox::Critical, "No new file selected", "No new file created. \nEdditing last opend file." );
			error.exec();
			return;
		}
	}
	OnClear();
}

bool GuiEditorModel::OpenNewFileDialog()
{
	NewFileDialog* newFileDialog = new NewFileDialog(pGlWidget);	
	newFileDialog->data->OnOptActivePath( activePath );
	int result = newFileDialog->exec();
	if(result)
	{
		activePath = newFileDialog->data->activePath;
		filename = newFileDialog->data->filename;
		OptActivePath(activePath);
		OptFileName(filename);
		return true;
	}
	return false;
}

/** EditableWidget methods */

void GuiEditorModel::OnWidgetSelected(EditableWidgetPtr widget)
{
	ActivateWidget(widget);
	if(state != MS_SELECT)
		AddWidget(widget);
}

void GuiEditorModel::ActivateWidget(EditableWidgetPtr widget)
{
	if(activeWidget)
		activeWidget->Deactivate();
	activeWidget = widget;
	if(activeWidget)
	{
		activeWidget->Activate();
		emit PropPosX(widget->data.x);
		emit PropPosY(widget->data.y);
		emit PropWidth(widget->data.w);
		emit PropHeight(widget->data.h);
		emit PropName(QString::fromStdString(widget->data.name));
		emit PropTexPath(QString::fromStdString(widget->data.texturePath));
		emit PropResizeChildren( widget->GetResizeChildren());
	}
	else
	{
		emit PropPosX(0);
		emit PropPosY(0);
		emit PropWidth(0);
		emit PropHeight(0);
		emit PropName(QString());
		emit PropTexPath(QString());
		emit PropResizeChildren(false);
	}
}

void GuiEditorModel::OnLayerClick(GuiSM::Events::MouseEvent* e)
{
	ActivateWidget(EditableWidgetPtr());
	AddWidget( EditableWidgetPtr() );
}

void GuiEditorModel::AddWidget(EditableWidgetPtr editableWidgetParent)
{
	if(state == MS_ADD_CUSTOM)
	{
		GUILoader::Load(customWidgetPath, editableWidgetParent);
		state = MS_SELECT;		
	}

	if(state == MS_SELECT)
		return;	

	EditableWidgetPtr newWidget;
	GuiSM::Widgets::WidgetPtr parent;

	if(editableWidgetParent)
		parent = editableWidgetParent->main;
	else
		parent = GuiSM::GUIManager::get().getActiveLayer();
	
	GuiSM::Rect r(0,0,50,50);
	r.x = GuiSM::GUIManager::get().getMouse().position.x - parent->getPosReal().x;
	r.y = GuiSM::GUIManager::get().getMouse().position.y - parent->getPosReal().y;	

	switch(state)
	{	
	case MS_ADD_WINDOW:	
		newWidget = EditableWidgetPtr(new EditableWidget(GuiSM::Widgets::EWT_WINDOW, r, editableWidgetParent));
		break;
	case MS_ADD_BUTTON:		
		newWidget = EditableWidgetPtr(new EditableWidget(GuiSM::Widgets::EWT_BUTTON, r, editableWidgetParent));
		break;
	case MS_ADD_PROGRESSBAR:		
		newWidget = EditableWidgetPtr(new EditableWidget(GuiSM::Widgets::EWT_PROGRESSBAR, r, editableWidgetParent));
		break;
	case MS_ADD_SCROLLBAR:		
		newWidget = EditableWidgetPtr(new EditableWidget(GuiSM::Widgets::EWT_SCROLL, r, editableWidgetParent));
		break;
	case MS_ADD_IMAGE:		
		newWidget = EditableWidgetPtr(new EditableWidget(GuiSM::Widgets::EWT_IMAGE, r, editableWidgetParent));
		break;
		
	}
	
	newWidget->SetName( GenerateName() );
	newWidget->SetParent(editableWidgetParent);

	state = MS_SELECT;

	ActivateWidget(newWidget);

	if(editableWidgetParent == NULL)
		widgets.push_back(newWidget);
	else
		editableWidgetParent->AddChild(newWidget);
	
}


QString GuiEditorModel::GenerateName()
{
	char name[128];
	std::string base;
	switch(state)
	{
	case MS_ADD_WINDOW:
		base = "GuiWindow";
		break;
	case MS_ADD_BUTTON:
		base = "GuiButton";
		break;
	case MS_ADD_PROGRESSBAR:
		base = "GuiProgressbar";
		break;
	case MS_ADD_SCROLLBAR:
		base = "GuiScrollbar";
		break;
	case MS_ADD_IMAGE:
		base = "GuiImage";
		break;
	default:
		base = "GuiElement";
		break;
	}
	++widgetNum[state];
	sprintf(name, "%s%02d", base.c_str(), widgetNum[state]);
	return QString::fromAscii(name);
}


void GuiEditorModel::OnClear()
{
	GuiSM::GUIManager::get().getActiveLayer()->focus();
	ActivateWidget(EditableWidgetPtr());
	foreach(EditableWidgetPtr widget, widgets)
	{
		widget->Delete();
	}
	widgets.clear();
	for(int i = 0; i < MS_MAX; ++i)
	{
		widgetNum[i] = 0;
	}
	
	

}

void GuiEditorModel::OnActiveWidgetMove()
{
	if(!activeWidget)
		return;
	emit PropPosX(activeWidget->data.x);
	emit PropPosY(activeWidget->data.y);
		
}

void GuiEditorModel::OnActiveWidgetResize()
{
	if(!activeWidget)
		return;
	emit PropWidth(activeWidget->data.w);
	emit PropHeight(activeWidget->data.h);
}
