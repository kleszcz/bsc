#include "GLView.h"
//#include "GuiSM/Button.h"
//#include "GuiSM/Window.h"


using namespace GuiSM;

#define MIN(a, b) a<b?a:b

GLView::GLView(void)
{
}


GLView::~GLView(void)
{
}

void GLView::OnInit()
{	
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


//	GUIManager& gmgr = GUIManager::get();
//	gmgr.addLayer("test");
//	gmgr.setActiveLayer("test");
	//Widgets::CSimpleButton::Create(Rect(10,10,100,100), gmgr.getActiveLayer());
	//Widgets::CSimpleWindow::Create(Rect(50,150,100,100), gmgr.getActiveLayer());
	
}

void GLView::OnPaint()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode (GL_MODELVIEW);
	GUIManager::get().render();	
	
}

void GLView::OnResize(int w, int h)
{
	if(h == 0)
		h = 1;
	float ratio = 1.0* w / h;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	gluPerspective(45,ratio,1,1000);
	glMatrixMode(GL_MODELVIEW);

	GUIManager::get().setSize( GuiSM::Point(w,h));

}