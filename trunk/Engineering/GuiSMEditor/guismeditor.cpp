#include "guismeditor.h"

GuiSMEditor::GuiSMEditor(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	pModel = new GuiEditorModel(ui.glWidget);
	pWelcomeDialog = new WelcomeDialog(ui.glWidget);


	/// Properties signals and slots
	connect(ui.propName			, SIGNAL(textChanged(QString))		, pModel		, SLOT(OnPropName(QString))		);
	connect(pModel				, SIGNAL(PropPosX(int))				, ui.propPosX	, SLOT(setValue(int))			);
	connect(pModel				, SIGNAL(PropPosY(int))				, ui.propPosY	, SLOT(setValue(int))			);
	connect(pModel				, SIGNAL(PropWidth(int))			, ui.propWidth	, SLOT(setValue(int))			);
	connect(pModel				, SIGNAL(PropHeight(int))			, ui.propHeight	, SLOT(setValue(int))			);
	connect(ui.propPosX			, SIGNAL(valueChanged(int))			, pModel		, SLOT(OnPropPosX(int))			);
	connect(ui.propPosY			, SIGNAL(valueChanged(int))			, pModel		, SLOT(OnPropPosY(int))			);
	connect(ui.propWidth		, SIGNAL(valueChanged(int))			, pModel		, SLOT(OnPropWidth(int))		);
	connect(ui.propHeight		, SIGNAL(valueChanged(int))			, pModel		, SLOT(OnPropHeight(int))		);
	connect(ui.propTexPathButton, SIGNAL(clicked())					, pModel		, SLOT(OnPropTexPathButton())	);
	connect(pModel				, SIGNAL(PropTexPath(QString))		, ui.propTexPath, SLOT(setText(QString))		);
	connect(pModel				, SIGNAL(PropName(QString))			, ui.propName	, SLOT(setText(QString))		);
	connect(ui.propDeleteButton	, SIGNAL(clicked())					, pModel		, SLOT(OnPropDeleteButton())	);
	connect(ui.propResizeChildren, SIGNAL(stateChanged(int))		, pModel		, SLOT(OnPropResizeChildren(int)));
	connect(pModel				, SIGNAL(PropResizeChildren(bool ))	, ui.propResizeChildren		, SLOT(setChecked(bool ))		);
		
	/// Options signals and slots
	connect(ui.optActivePath		, SIGNAL(textChanged(QString))		, pModel			, SLOT(OnOptActivePath(QString))	);
	connect(pModel					, SIGNAL(OptActivePath(QString))	, ui.optActivePath	, SLOT(setText(QString))			);
	connect(ui.optActivePathButton	, SIGNAL(clicked())					, pModel			, SLOT(OnOptActivePathButton())		);
	connect(ui.optFileName			, SIGNAL(textChanged(QString))		, pModel			, SLOT(OnOptFileName(QString))		);
	connect(pModel					, SIGNAL(OptFileName(QString))		, ui.optFileName	, SLOT(setText(QString))			);
	/// Actions signals and slots
	connect(ui.actionAdd_Window		, SIGNAL(triggered())				, pModel	, SLOT(OnActAddWindow())	);
	connect(ui.actionAdd_Button		, SIGNAL(triggered())				, pModel	, SLOT(OnActAddButton())	);
	connect(ui.actionAdd_Progressbar, SIGNAL(triggered())				, pModel	, SLOT(OnActAddProgressbar())	);
	connect(ui.actionAdd_Scrollbar	, SIGNAL(triggered())				, pModel	, SLOT(OnActAddScrollbar())	);
	connect(ui.actionAdd_Custom_Widget	, SIGNAL(triggered())				, pModel	, SLOT(OnActAddCustomWidget())	);
	connect(ui.actionSave			, SIGNAL(triggered())				, pModel	, SLOT(OnActSave())			);
	connect(ui.actionLoad			, SIGNAL(triggered())				, pModel	, SLOT(OnActLoad())			);
	connect(ui.actionNew			, SIGNAL(triggered())				, pModel	, SLOT(OnActNew())			);

	//Welcome dialog signals
	

	connect(pWelcomeDialog	, SIGNAL(New())		, pModel	, SLOT(OnActNew())	);
	connect(pWelcomeDialog	, SIGNAL(Load())	, pModel	, SLOT(OnActLoad())	);
	
	int result = pWelcomeDialog->exec();
	if(!result)
	{
		exit(0);
	}

}

GuiSMEditor::~GuiSMEditor()
{
	delete pModel;
	delete pWelcomeDialog;
}
