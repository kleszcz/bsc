#pragma once


#include <qobject.h>

//#include "GuiLoader.h"
#include "WidgetData.h"

#include <GuiSM/Widget.h>
#include <GuiSM/Window.h>
#include <GuiSM/Button.h>
#include <GuiSM/Layer.h>
#include <GuiSM/Image.h>
#include <GuiSM/Progressbar.h>
#include <GuiSM/Scrollbar.h>


#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <vector>
#include <pugixml/pugixml.hpp>

class GuiEditorModel;

class EditableWidget;

typedef boost::shared_ptr<EditableWidget> EditableWidgetPtr;

class EditableWidget : public boost::enable_shared_from_this<EditableWidget>
{


public:
	EditableWidget(void);
	EditableWidget(GuiSM::Widgets::WidgetType type, GuiSM::Rect rect, EditableWidgetPtr parent);
	~EditableWidget(void);

protected:
	void OnClick(GuiSM::Events::MouseEvent* e);
	void OnMoveButtonUp(GuiSM::Events::MouseButtonEvent* e);
	void OnMoveButtonDown(GuiSM::Events::MouseButtonEvent* e);
	void OnMoveWidget(GuiSM::Events::MouseMoveEvent* e);
	void OnResizeButtonUp(GuiSM::Events::MouseButtonEvent* e);
	void OnResizeButtonDown(GuiSM::Events::MouseButtonEvent* e);
	void OnResizeWidget(GuiSM::Events::MouseMoveEvent* e);

public:
	void SetPosX(int x);
	void SetPosY(int y);
	void SetWidth(int width);
	void SetHeight(int height);
	void SetTexture(QString path);
	void SetName(QString name);
	void SetParent(EditableWidgetPtr parent);
	void SetResizeChildren(bool resize);
	void SetDeepResizeChildren(bool resize);

	EditableWidgetPtr GetParent() const;
	bool GetResizeChildren() const;

	void ResizeByRatio(double xRatio, double yRatio);
	

	void AddChild(EditableWidgetPtr widget);
	void ToXml(pugi::xml_node root);

	void Activate();
	void Deactivate();

	void Delete();
protected:
	void CreateMainWidget(GuiSM::Widgets::WidgetType type, GuiSM::Rect rect, EditableWidgetPtr parent);
	void CreateHelpers(int size);
	void InitMouseActions();
	void InitData(GuiSM::Widgets::WidgetType type, GuiSM::Rect rect, EditableWidgetPtr parent);

	void AfterResize();

public:
		WidgetData data;
		
public:
	static GuiEditorModel* model;

public:
	GuiSM::Widgets::WidgetPtr main;

protected:
	GuiSM::Widgets::WidgetPtr resize;
	GuiSM::Widgets::WidgetPtr move;
	std::vector<EditableWidgetPtr> children;
	EditableWidgetPtr parent;
	bool dragging;
	bool active;
	bool resizing;	
	bool resizeChildren;
	int handleSize;

	double x, y;
};


