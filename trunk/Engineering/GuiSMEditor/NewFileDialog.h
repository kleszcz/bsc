#pragma once
#include <qdialog.h>
#include <qobject.h>
#include "ui_newFileDialog.h"
#include <qstring.h>
#include <qfiledialog.h>

class NewFileDialog;

class NewFileDialogData :  public QObject
{
	Q_OBJECT;
public:
	NewFileDialogData(NewFileDialog* dialog);
	~NewFileDialogData();

	QString filename;
	QString activePath;

	NewFileDialog* parentDialog;

public slots:
	void OnOptActivePath(QString path);
	void OnOptActivePathButton();
	void OnOptFileName(QString name);

signals:
		void OptFileName(QString name);
		void OptActivePath(QString path);
};

class NewFileDialog :
	public QDialog
{
public:
	NewFileDialog(QWidget *parent = 0, Qt::WFlags flags = 0);
	~NewFileDialog(void);
			
	Ui::NewFileDialog ui;
	NewFileDialogData* data;

};

