#pragma once
#include <GuiSM/GUIManager.h>
#include <GL/gl.h>
#include <GL/glu.h>

class GLView
{
public:
	GLView(void);
	~GLView(void);

	void OnInit();
	void OnPaint();
	void OnResize(int width, int height);
};

