/****************************************************************************
** Meta object code from reading C++ file 'GuiEditorModel.h'
**
** Created: Sun 27. Jan 18:01:19 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../GuiEditorModel.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GuiEditorModel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GuiEditorModel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   16,   15,   15, 0x05,
      34,   32,   15,   15, 0x05,
      54,   48,   15,   15, 0x05,
      76,   69,   15,   15, 0x05,
      97,   92,   15,   15, 0x05,
     123,  118,   15,   15, 0x05,
     147,  141,   15,   15, 0x05,
     172,   92,   15,   15, 0x05,
     195,  118,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
     216,  118,   15,   15, 0x0a,
     236,   16,   15,   15, 0x0a,
     252,   32,   15,   15, 0x0a,
     268,   48,   15,   15, 0x0a,
     285,   69,   15,   15, 0x0a,
     303,   15,   15,   15, 0x0a,
     325,   92,   15,   15, 0x0a,
     348,   15,   15,   15, 0x0a,
     369,  141,   15,   15, 0x0a,
     395,   92,   15,   15, 0x0a,
     420,   15,   15,   15, 0x0a,
     444,  118,   15,   15, 0x0a,
     467,   15,   15,   15, 0x0a,
     484,   15,   15,   15, 0x0a,
     501,   15,   15,   15, 0x0a,
     523,   15,   15,   15, 0x0a,
     543,   15,   15,   15, 0x0a,
     559,   15,   15,   15, 0x0a,
     582,   15,   15,   15, 0x0a,
     594,   15,   15,   15, 0x0a,
     605,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GuiEditorModel[] = {
    "GuiEditorModel\0\0x\0PropPosX(int)\0y\0"
    "PropPosY(int)\0width\0PropWidth(int)\0"
    "height\0PropHeight(int)\0path\0"
    "PropTexPath(QString)\0name\0PropName(QString)\0"
    "state\0PropResizeChildren(bool)\0"
    "OptActivePath(QString)\0OptFileName(QString)\0"
    "OnPropName(QString)\0OnPropPosX(int)\0"
    "OnPropPosY(int)\0OnPropWidth(int)\0"
    "OnPropHeight(int)\0OnPropTexPathButton()\0"
    "OnPropTexPath(QString)\0OnPropDeleteButton()\0"
    "OnPropResizeChildren(int)\0"
    "OnOptActivePath(QString)\0"
    "OnOptActivePathButton()\0OnOptFileName(QString)\0"
    "OnActAddWindow()\0OnActAddButton()\0"
    "OnActAddProgressbar()\0OnActAddScrollbar()\0"
    "OnActAddImage()\0OnActAddCustomWidget()\0"
    "OnActSave()\0OnActNew()\0OnActLoad()\0"
};

void GuiEditorModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GuiEditorModel *_t = static_cast<GuiEditorModel *>(_o);
        switch (_id) {
        case 0: _t->PropPosX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->PropPosY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->PropWidth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->PropHeight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->PropTexPath((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->PropName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->PropResizeChildren((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->OptActivePath((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->OptFileName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->OnPropName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->OnPropPosX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->OnPropPosY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->OnPropWidth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->OnPropHeight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->OnPropTexPathButton(); break;
        case 15: _t->OnPropTexPath((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 16: _t->OnPropDeleteButton(); break;
        case 17: _t->OnPropResizeChildren((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->OnOptActivePath((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 19: _t->OnOptActivePathButton(); break;
        case 20: _t->OnOptFileName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 21: _t->OnActAddWindow(); break;
        case 22: _t->OnActAddButton(); break;
        case 23: _t->OnActAddProgressbar(); break;
        case 24: _t->OnActAddScrollbar(); break;
        case 25: _t->OnActAddImage(); break;
        case 26: _t->OnActAddCustomWidget(); break;
        case 27: _t->OnActSave(); break;
        case 28: _t->OnActNew(); break;
        case 29: _t->OnActLoad(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData GuiEditorModel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GuiEditorModel::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_GuiEditorModel,
      qt_meta_data_GuiEditorModel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GuiEditorModel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GuiEditorModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GuiEditorModel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GuiEditorModel))
        return static_cast<void*>(const_cast< GuiEditorModel*>(this));
    return QObject::qt_metacast(_clname);
}

int GuiEditorModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    }
    return _id;
}

// SIGNAL 0
void GuiEditorModel::PropPosX(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void GuiEditorModel::PropPosY(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void GuiEditorModel::PropWidth(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void GuiEditorModel::PropHeight(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void GuiEditorModel::PropTexPath(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void GuiEditorModel::PropName(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void GuiEditorModel::PropResizeChildren(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void GuiEditorModel::OptActivePath(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void GuiEditorModel::OptFileName(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_END_MOC_NAMESPACE
