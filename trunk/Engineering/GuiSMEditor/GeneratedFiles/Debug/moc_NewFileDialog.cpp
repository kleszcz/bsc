/****************************************************************************
** Meta object code from reading C++ file 'NewFileDialog.h'
**
** Created: Sun 27. Jan 18:01:19 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../NewFileDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'NewFileDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_NewFileDialogData[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      24,   19,   18,   18, 0x05,
      50,   45,   18,   18, 0x05,

 // slots: signature, parameters, type, tag, flags
      73,   45,   18,   18, 0x0a,
      98,   18,   18,   18, 0x0a,
     122,   19,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_NewFileDialogData[] = {
    "NewFileDialogData\0\0name\0OptFileName(QString)\0"
    "path\0OptActivePath(QString)\0"
    "OnOptActivePath(QString)\0"
    "OnOptActivePathButton()\0OnOptFileName(QString)\0"
};

void NewFileDialogData::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NewFileDialogData *_t = static_cast<NewFileDialogData *>(_o);
        switch (_id) {
        case 0: _t->OptFileName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->OptActivePath((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->OnOptActivePath((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->OnOptActivePathButton(); break;
        case 4: _t->OnOptFileName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData NewFileDialogData::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject NewFileDialogData::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_NewFileDialogData,
      qt_meta_data_NewFileDialogData, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NewFileDialogData::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NewFileDialogData::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NewFileDialogData::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NewFileDialogData))
        return static_cast<void*>(const_cast< NewFileDialogData*>(this));
    return QObject::qt_metacast(_clname);
}

int NewFileDialogData::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void NewFileDialogData::OptFileName(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void NewFileDialogData::OptActivePath(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
