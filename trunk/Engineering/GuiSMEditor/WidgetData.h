#pragma once
#include <GuiSM/GUIManager.h>
#include <GuiSM/Widget.h>
/// Widget definition structure

/// \author Jan Kleszczyński
struct WidgetData
{	
	/// Defoult constructor.
	WidgetData() : 
		type(GuiSM::Widgets::EWT_NONE),
		x(0),
		y(0),
		w(0),
		h(0)
	{
		texturePath.clear();
		parentName.clear();
		name.clear();
	}

	/// Type of widget. Same as widget template parameter.
	GuiSM::Widgets::WidgetType type;
	/// Postion x
	int x;
	/// Position y
	int y;
	/// Width
	int w;
	/// Height
	int h;
	/// Path to texture file
	std::string texturePath;
	/// Name of parent widget
	std::string parentName;
	/// Name of widget
	std::string name;
};
