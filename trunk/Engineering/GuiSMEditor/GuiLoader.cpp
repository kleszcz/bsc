#include <pugixml/pugixml.hpp>
#include <GuiEditorModel.h>
#include "GuiLoader.h"
#include "Exceptions.h"


/// GUILoader class implementation
GuiEditorModel* GUILoader::model = NULL;

bool GUILoader::Load(std::string sPath, EditableWidgetPtr pParent)
{
	pugi::xml_document doc;
	EditableWidgetPtr firstWidget;
	bool firstSet = false;
	doc.load_file(sPath.c_str());
	pugi::xml_node root = doc.child("vguismd");
	if(!root )
		//return false;
		throw IncorectFileFormatException();

	EditableWidgetPtr pWidget;

	pugi::xml_node widget = root.child("widget");
	while(widget)
	{
		WidgetData wd;
		wd.name			=	std::string( widget.attribute("name").value() );
		wd.type			=	GuiSM::Widgets::WidgetType( widget.attribute("type").as_int() );
		wd.x			=	widget.child("position").attribute("x").as_int();
		wd.y			=	widget.child("position").attribute("y").as_int();
		wd.w			= 	widget.child("size").attribute("w").as_int();
		wd.h			=	widget.child("size").attribute("h").as_int();
		wd.texturePath	=	std::string( widget.attribute("texture_path").value() );
		
		pWidget = AddWidget(wd, pParent);

		if(!firstSet && pWidget)
		{
			firstSet = true;
			firstWidget = pWidget;
		}
		if(widget.child("widget"))
		{
			pParent = pWidget;
			widget = widget.child("widget");
		}
		else if(widget.next_sibling())
		{
			widget = widget.next_sibling();
		}
		else
		{			
			//pParent = pWidget->getParent()->getParent();
			widget = widget.parent().next_sibling();
		}		
	}
	model->ActivateWidget(firstWidget);
	return true;
}

EditableWidgetPtr GUILoader::AddWidget(WidgetData& widget, EditableWidgetPtr editableWidgetParent)
{
	EditableWidgetPtr newWidget;
	GuiSM::Widgets::WidgetPtr parent;
	GuiSM::Rect r;

	r.x = widget.x;
	r.y = widget.y;
	r.width = widget.w;
	r.height = widget.h;	

	
		
	newWidget = EditableWidgetPtr(new EditableWidget(widget.type, r, editableWidgetParent));
	
        if(!widget.texturePath.empty() && widget.type != GuiSM::Widgets::EWT_IMAGE)
	{
		GuiSM::TextureManager& tm = GuiSM::TextureManager::get();
		GuiSM::Texture* t = tm.getTexture(widget.texturePath);
		newWidget->main->setTexture(*t);
	}
	if(!widget.name.empty())
		newWidget->SetName(QString::fromStdString(widget.name));
		
		
	if(editableWidgetParent == NULL)
		model->widgets.push_back(newWidget);
	else
		editableWidgetParent->AddChild(newWidget);

	newWidget->data.texturePath = widget.texturePath;
	newWidget->data.parentName = widget.parentName;
	newWidget->data.name = widget.name;
	newWidget->data.x = widget.x;
	newWidget->data.y = widget.y;
	newWidget->data.h = widget.h;
	newWidget->data.w = widget.w;
	newWidget->data.type = widget.type;


	return newWidget;
}

