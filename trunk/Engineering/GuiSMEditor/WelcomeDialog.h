#pragma once
#include <qdialog.h>
#include "ui_welcomeDialog.h"



class WelcomeDialog : public QDialog
{
	Q_OBJECT;
public:
	WelcomeDialog(QWidget* parent = 0, Qt::WFlags flags = Qt::WindowTitleHint );
	~WelcomeDialog(void);

public slots:
	void OnOpenDialog();
	void OnNew();
	void OnLoad();
	void OnExit();

signals:
	void New();
	void Load();
//	void Exit();


protected:
	Ui::WelcomeDialog ui;


};

