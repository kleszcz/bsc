#include "WelcomeDialog.h"


WelcomeDialog::WelcomeDialog(QWidget* parent, Qt::WFlags flags)
{
	ui.setupUi(this);

	connect( ui.newButton, SIGNAL(clicked()), this, SLOT(OnNew()));
	connect( ui.loadButton, SIGNAL(clicked()), this, SLOT(OnLoad()));
	connect( ui.exitButton, SIGNAL(clicked()), this, SLOT(OnExit()));
}


WelcomeDialog::~WelcomeDialog(void)
{
}

void WelcomeDialog::OnOpenDialog()	
{
	this->exec();
}

void WelcomeDialog::OnNew()			
{
	emit New();
	this->done(1);
}

void WelcomeDialog::OnLoad()		
{
	emit Load();
	this->done(2);
}

void WelcomeDialog::OnExit()		
{
	//emit Exit();
	exit(0);
}

