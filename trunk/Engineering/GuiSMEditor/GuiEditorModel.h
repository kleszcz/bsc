#pragma once

#include <GuiSM/GUIManager.h>
#include <GuiSM/TextureManager.h>
#include <GuiSM/Widget.h>
#include <qobject.h>
#include <QDate>
#include <qfiledialog.h>
#include <vector>
#include <pugixml/pugixml.hpp>

#include "GLWidget.h"
#include "EditableWidget.h"
#include "GuiLoader.h"


#include "NewFileDialog.h"


class GuiEditorModel : public QObject
{
	Q_OBJECT;
public:
	GuiEditorModel(GLWidget* glWidget);
	~GuiEditorModel(void);

	

public:
	enum EModelState
	{
		MS_SELECT,
		MS_ADD_BUTTON,
		MS_ADD_WINDOW,
		MS_ADD_PROGRESSBAR,
		MS_ADD_SCROLLBAR,
		MS_ADD_IMAGE,

		MS_ADD_CUSTOM,
		MS_MAX
	};


/// Properties slots
public slots:
	void OnPropName(QString name);
	void OnPropPosX(int x);
	void OnPropPosY(int y);
	void OnPropWidth(int width);
	void OnPropHeight(int height);
	void OnPropTexPathButton();
	void OnPropTexPath(QString path);
	void OnPropDeleteButton();
	void OnPropResizeChildren(int state);

/// Properties signals 
signals:
	void PropPosX(int x);
	void PropPosY(int y);
	void PropWidth(int width);
	void PropHeight(int height);
	void PropTexPath(QString path);
	void PropName(QString name);
	void PropResizeChildren(bool state);

/// Options slots
public slots:
	void OnOptActivePath(QString path);
	void OnOptActivePathButton();
	void OnOptFileName(QString name);
/// Options signals
signals:
	void OptActivePath(QString path);
	void OptFileName(QString name);

/// Actions slots
public slots:
	void OnActAddWindow();
	void OnActAddButton();
	void OnActAddProgressbar();
	void OnActAddScrollbar();
	void OnActAddImage();
	void OnActAddCustomWidget();
	void OnActSave();
	void OnActNew();
	void OnActLoad();

public:
	void OnWidgetSelected(EditableWidgetPtr widget);
	void OnClear();
	void OnActiveWidgetMove();
	void OnActiveWidgetResize();
	void ActivateWidget(EditableWidgetPtr widget);
	QString GetActivePath() const { return activePath; }

protected:
	void OnLayerClick(GuiSM::Events::MouseEvent* e);
	void AddWidget(EditableWidgetPtr editableWidgetParent);
	
	QString GenerateName();
	bool OpenNewFileDialog();


public:
	EditableWidgetPtr activeWidget;
	std::vector<EditableWidgetPtr> widgets;

private:
	GLWidget* pGlWidget;
	QString activePath;
	QString filename;
	EModelState state;
	unsigned int widgetNum[MS_MAX];
	std::string customWidgetPath;
	
	
};

