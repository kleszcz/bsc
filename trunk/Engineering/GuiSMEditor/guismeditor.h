#ifndef GUISMEDITOR_H
#define GUISMEDITOR_H

#include <QtGui/QMainWindow>
#include "ui_guismeditor.h"
#include "GuiEditorModel.h"
#include "WelcomeDialog.h"

class GuiSMEditor : public QMainWindow
{
	Q_OBJECT

public:
	GuiSMEditor(QWidget *parent = 0, Qt::WFlags flags = 0);
	~GuiSMEditor();

private:
	Ui::GuiSMEditorClass ui;
	GuiEditorModel* pModel;
	WelcomeDialog* pWelcomeDialog;
	
};

#endif // GUISMEDITOR_H
