#pragma once
#include <qstring.h>
#include <qstringlist.h>
#include <qdir.h>
#include <string>

class Utility
{
public:

	static std::string makePathRelativeTo(QString path, QString relativeTo)
	{
		if(path == "")
			return std::string("");
		QString relativePath;
		QDir dir(relativeTo);
		QDir pathDir(path);
		pathDir.makeAbsolute();
		path = pathDir.path();
		relativePath = dir.relativeFilePath(path);
	
		return relativePath.toStdString();
	}
	
	static QString getPath(QString pathWithFileName)
	{
		QString path;
		QString delim = "/";
		QStringList list = pathWithFileName.split(delim);
		if(!list.isEmpty())
			list.erase(list.end() - 1);
		path = list.join(delim);
	
		return path;
	}

	static QString getFileName(QString pathWithFileName)
	{
		QDir dir(pathWithFileName);
		QString path = dir.dirName();
		QStringList list;
		QString delim = ".";
		list = path.split(delim);
		if(!list.isEmpty())
			list.erase(list.end()-1);
		path = list.join(delim);
	
		return path;
	}
};

