# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


HEADERS += ./Exceptions.h \
    ./Utility.h \
    ./WidgetData.h \
    ./GLView.h \
    ./GuiLoader.h \
    ./guismeditor.h \
    ./WelcomeDialog.h \
    ./EditableWidget.h \
    ./NewFileDialog.h \
    ./GuiEditorModel.h \
    ./GLWidget.h
SOURCES += ./EditableWidget.cpp \
    ./GLView.cpp \
    ./GLWidget.cpp \
    ./GuiEditorModel.cpp \
    ./GuiLoader.cpp \
    ./guismeditor.cpp \
    ./main.cpp \
    ./NewFileDialog.cpp \
    ./WelcomeDialog.cpp
FORMS += ./guismeditor.ui \
    ./newFileDialog.ui \
    ./welcomeDialog.ui
RESOURCES += guismeditor.qrc
