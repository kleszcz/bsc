#pragma once

#include <string>
#include <vector>

#include <GuiSM/GUIManager.h>

#include "WidgetData.h"
#include "EditableWidget.h"



/*// Any new widget should be included here
#include <GuiSM\Window.h>
#include <GuiSM\Button.h>
#include <GuiSM\Layer.h>
#include <GuiSM\Image.h>
#include <GuiSM\Progressbar.h>
#include <GuiSM\Scrollbar.h>
#include <GuiSM\Widget.h>
//#include <GuiSM\CheckBox.h>
/////////////////////////////////////////////
*/


class GuiEditorModel;

/// Loads and runs gui from xml.

/// \author Jan Kleszczyński
class GUILoader
{
public:
	/// Loads widgets from xml. Set  as written in xml file.
	/// \param sPath std::string xml document path
	/// \param pParent GuiSM::Widgets::WidgetPtr parent for gui component (usually a layer)
	/// \retval bool true if succeeded
	static bool Load(std::string sPath, EditableWidgetPtr pParent = EditableWidgetPtr());

	static GuiEditorModel* model;
	
private:
	/// Creats widget and adds them to GUIManager setting size, texture, parent and name.
	/// \param WidgetData widget definition to add to the list
	/// \param pParent GuiSM::Widgets::WidgetPtr widget parent
	static EditableWidgetPtr AddWidget(WidgetData& widget, EditableWidgetPtr pRoot);

};