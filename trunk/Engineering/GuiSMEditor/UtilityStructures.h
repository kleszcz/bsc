#pragma once
#include <qstring.h>

struct Properties
{
	QString name;
	QString texturePath;
	int x;
	int y;
	int width;
	int height;

	Properties() : name(QString("")),
		texturePath(QString("")),
		x(0),
		y(0),
		width(0),
		height(0)
	{}
};


struct Options
{
	QString activePath;
}
