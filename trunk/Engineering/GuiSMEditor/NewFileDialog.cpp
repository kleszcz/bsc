#include "NewFileDialog.h"


NewFileDialog::NewFileDialog(QWidget *parent, Qt::WFlags flags) : QDialog(parent, flags)
{
	ui.setupUi(this);
	data = new NewFileDialogData(this);

}


NewFileDialog::~NewFileDialog(void)
{
	delete data;
}


void NewFileDialogData::OnOptActivePath(QString path)
{
	activePath = path;
	emit OptActivePath(path);
}

void NewFileDialogData::OnOptActivePathButton()
{
	QString path = QFileDialog::getExistingDirectory(parentDialog, tr("Set active directory"), activePath, QFileDialog::ShowDirsOnly);
	if(path == "")
		return;
	emit OptActivePath(path);
	
	activePath = path;
}

void NewFileDialogData::OnOptFileName(QString name)
{
	filename = name;
	emit OptFileName(name);
}

NewFileDialogData::NewFileDialogData(NewFileDialog* dialog) : parentDialog(dialog)
{

	connect(parentDialog->ui.optActivePath		, SIGNAL(textChanged(QString))		, this				, SLOT(OnOptActivePath(QString))	);
	connect(this					, SIGNAL(OptActivePath(QString))	, parentDialog->ui.optActivePath	, SLOT(setText(QString))			);
	connect(parentDialog->ui.optActivePathButton	, SIGNAL(clicked())					, this				, SLOT(OnOptActivePathButton())		);
	connect(parentDialog->ui.optFileName			, SIGNAL(textChanged(QString))		, this				, SLOT(OnOptFileName(QString))		);
	connect(this					, SIGNAL(OptFileName(QString))		, parentDialog->ui.optFileName	, SLOT(setText(QString))			);
}

NewFileDialogData::~NewFileDialogData()
{
}