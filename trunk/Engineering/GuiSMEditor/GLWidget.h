#pragma once
#include <QGLWidget>
#include <QMouseEvent>
#include <QPaintEvent>
#include <qtimer.h>
//#include <GuiSM/GUIManager.h>

#include "GLView.h"

class GLWidget :
	public QGLWidget
{
	Q_OBJECT

public:
	GLWidget(QWidget *parent = 0);
	~GLWidget(void);
	QSize minimumSizeHint() const;
    QSize sizeHint() const;



protected slots:
	void OnTimer();
	
protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

	/// Mouse events
	virtual void mouseMoveEvent(QMouseEvent* );
	virtual void mousePressEvent(QMouseEvent* );
	virtual void mouseReleaseEvent(QMouseEvent* );


protected:
	QColor qtBlack;
	GLView* glView;
	QTimer* qTimer;
};

