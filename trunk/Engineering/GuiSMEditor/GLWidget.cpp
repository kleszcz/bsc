#include "GLWidget.h"




GLWidget::GLWidget(QWidget *parent) : QGLWidget(QGLFormat(QGL::SampleBuffers),parent)
{	

	if(!hasMouseTracking())
		setMouseTracking(true);

        qtBlack = QColor::fromRgba(QRgb(Qt::black));
	glView = new GLView();
	qTimer = new QTimer();
	qTimer->setInterval(50);
	
	connect( qTimer, SIGNAL(timeout()), this, SLOT(OnTimer()) );

	qTimer->start();

}


GLWidget::~GLWidget(void)
{
	 delete glView;
}

QSize GLWidget::minimumSizeHint() const
 {
     return QSize(16777215, 16777215);
 }

 QSize GLWidget::sizeHint() const
 {
     return QSize(16777215, 16777215);
 }

  void GLWidget::initializeGL()
 {     
	 glView->OnInit();	
 }

 void GLWidget::paintGL()
 {
	 glView->OnPaint();
	 swapBuffers();
 }

 void GLWidget::resizeGL(int width, int height)
 {
	 glView->OnResize(width, height);
 }


 void GLWidget::mouseMoveEvent(QMouseEvent* mouseEvent)
 {
	 int x = mouseEvent->pos().x();
	 int y = mouseEvent->pos().y();
	 GuiSM::GUIManager::get().mouseMove(x,y);
	 paintGL();
 }
 
 void GLWidget::mousePressEvent(QMouseEvent* mouseEvent)
 {
	 Qt::MouseButton button = mouseEvent->button();
	 QPoint pos = mouseEvent->pos();
	 GuiSM::EMOUSEBUTTON smButton;
	 switch(button)
	 {
         case Qt::LeftButton:
		 smButton = GuiSM::EMB_LMB;
		 break;
        case Qt::MidButton:
		 smButton = GuiSM::EMB_MMB;
		 break;
         case Qt::RightButton:
		 smButton = GuiSM::EMB_RMB;
		 break;		 
	 }
	 GuiSM::GUIManager::get().mousePress(smButton, pos.x(), pos.y());
	 paintGL();
	
 }

 void GLWidget::mouseReleaseEvent(QMouseEvent* mouseEvent)
 {
	 Qt::MouseButton button = mouseEvent->button();
	 QPoint pos = mouseEvent->pos();
	 GuiSM::EMOUSEBUTTON smButton;
	 switch(button)
	 {
         case Qt::LeftButton:
		 smButton = GuiSM::EMB_LMB;
		 break;
        case Qt::MidButton:
		 smButton = GuiSM::EMB_MMB;
		 break;
         case Qt::RightButton:
		 smButton = GuiSM::EMB_RMB;
		 break;		 
	 }

	 GuiSM::GUIManager::get().mouseClick(smButton, pos.x(), pos.y());
	 GuiSM::GUIManager::get().mouseRelease(smButton, pos.x(), pos.y());
	paintGL();
 }

 void GLWidget::OnTimer()
 {	
	 paintGL();
 }


