#include "EditableWidget.h"
#include <GuiSM/FastDelegate/FastDelegate.h>
#include <GuiSM/FastDelegate/FastDelegateBind.h>

#include <GuiEditorModel.h>

#include "Exceptions.h"
#include "Utility.h"

#include <cmath>

using namespace GuiSM;
using namespace GuiSM::Widgets;

GuiEditorModel* EditableWidget::model = NULL;

/**
 * Constructors 
 */

EditableWidget::EditableWidget(GuiSM::Widgets::WidgetType type, GuiSM::Rect rect, EditableWidgetPtr parent) : active(false),
	dragging(false), resizing(false), resizeChildren(false)
{
	CreateMainWidget(type, rect, parent);
	if(!main)
		throw IncorectFileFormatException();
	CreateHelpers(20);
	InitMouseActions();
	InitData(type, rect, parent);
	Deactivate();
}

EditableWidget::~EditableWidget(void)
{
}

/**
 * Inits
 */


void EditableWidget::InitData(GuiSM::Widgets::WidgetType type, GuiSM::Rect rect, EditableWidgetPtr parent)
{
	if(parent)
		data.parentName = parent->main->getName();
	else
		data.parentName = "";
	data.type = type;
	data.x = rect.x;
	data.y = rect.y;
	data.w = rect.width;
	data.h = rect.height;
	
}

void EditableWidget::InitMouseActions()
{
	fastdelegate::FastDelegate< void(Events::MouseEvent* ) > delegate;
	delegate.bind(this, &EditableWidget::OnClick);		
	main->attach( delegate, Events::EET_MOUSECLICK);

	fastdelegate::FastDelegate< void(Events::MouseButtonEvent* ) > delegateButton;

	delegateButton.bind(this, &EditableWidget::OnMoveButtonDown);
	move->attach( delegateButton, Events::EET_MOUSEBUTTONDOWN);

	delegateButton.bind(this, &EditableWidget::OnMoveButtonUp);
	move->attach( delegateButton, Events::EET_MOUSEBUTTONUP);

	delegateButton.bind(this, &EditableWidget::OnResizeButtonDown);
	resize->attach( delegateButton, Events::EET_MOUSEBUTTONDOWN);

	delegateButton.bind(this, &EditableWidget::OnResizeButtonUp);
	resize->attach( delegateButton, Events::EET_MOUSEBUTTONUP);


	
	fastdelegate::FastDelegate< void(Events::MouseMoveEvent* ) > delegateMove;

	delegateMove.bind(this, &EditableWidget::OnMoveWidget);
	move->attach( delegateMove, Events::EET_MOUSEMOVE);

	delegateMove.bind(this, &EditableWidget::OnResizeWidget);
	resize->attach( delegateMove, Events::EET_MOUSEMOVE);
	
}


/**
 * Mouse actions
 */

/// Common mouse actions
void EditableWidget::OnClick(GuiSM::Events::MouseEvent* e)
{
	dragging = false;
	model->OnWidgetSelected(shared_from_this());
}

/// Move button
void EditableWidget::OnMoveButtonDown(GuiSM::Events::MouseButtonEvent* e)
{
        if(e->button == EMB_LMB && active)
		dragging = true;
}

void EditableWidget::OnMoveButtonUp(GuiSM::Events::MouseButtonEvent* e)
{
	dragging = false;
}

void EditableWidget::OnMoveWidget(GuiSM::Events::MouseMoveEvent* e)
{
	if(dragging)
	{
		Point pos = main->getPos() +e->current -e->previous;
		main->setPos( pos);
		data.x = pos.x;
		data.y = pos.y;

		if(model)
			model->OnActiveWidgetMove();
	}
}

/// Resize button
void EditableWidget::OnResizeButtonUp(GuiSM::Events::MouseButtonEvent* e)
{
	resizing = false;

	AfterResize();
}

void EditableWidget::OnResizeButtonDown(GuiSM::Events::MouseButtonEvent* e)
{
        if(e->button == EMB_LMB && active)
		resizing = true;

	y = main->getSize().height;
	x = main->getSize().width;
	
	
	
}

void EditableWidget::OnResizeWidget(GuiSM::Events::MouseMoveEvent* e)
{
	if(resizing)
	{
		Rect size = main->getSize();
		Point diff =  e->current -e->previous;

		size.height += diff.y;
		size.width += diff.x;

		if(size.height < 15 )
		{
			size.height = 15;
			resizing = false;
		}

		if(size.width < 15)
		{
			size.width = 15;
			resizing = false;			
		}
		
		int y_pre = main->getSize().height;
		int x_pre = main->getSize().width;

		main->setSize(size);

		int y = main->getSize().height;
		int x = main->getSize().width;
		resize->setPos(Point(x-handleSize, y-handleSize));

		data.h = main->getSize().height;
		data.w = main->getSize().width;

		if(model)
			model->OnActiveWidgetResize();

		if(!resizing)
			AfterResize();
/*		
		double xRatio = (double)x / (double)x_pre;
		double yRatio = (double)y / (double)y_pre;
		if(resizeChildren)
		{
			for(int i = 0; i < children.size(); ++i)
			{
				EditableWidgetPtr pWidget = children[i];
				pWidget->ResizeByRatio(xRatio, yRatio);
			}
		}
		*/
	}
}

/**
 * Creators
 */

void EditableWidget::CreateMainWidget(GuiSM::Widgets::WidgetType type, GuiSM::Rect rect, EditableWidgetPtr parent)
{
	GuiSM::Widgets::WidgetPtr parentGui;
	
	if(parent)
		parentGui = parent->main;
	else
		parentGui = GuiSM::GUIManager::get().getActiveLayer();

	switch(type)
	{
						
        case GuiSM::Widgets::EWT_BUTTON:
		main = GuiSM::Widgets::CSimpleButton::Create(rect, parentGui );
		break;
			
        case GuiSM::Widgets::EWT_CHECKBOX:
		main = GuiSM::Widgets::CSimpleCheckBox::Create(parentGui);
		main->setSize(rect);
		break;

        case GuiSM::Widgets::EWT_EDIT:
		main = GuiSM::Widgets::CSimpleEdit::Create(parentGui);
		main->setSize(rect);
		break;

        case GuiSM::Widgets::EWT_IMAGE:
		main = GuiSM::Widgets::CSimpleImage::Create(rect, parentGui);
		break;

        case GuiSM::Widgets::EWT_LAYER:
		//pWidget = GuiSM::Widgets::CSimpleLayer::Create(r, parentGui );
		//GuiSM::GUIManager::get().addLayer(widget.name);
		break;

        case GuiSM::Widgets::EWT_PROGRESSBAR:
		main = GuiSM::Widgets::CSimpleProgressBar::Create(rect, parentGui);
		break;

        case GuiSM::Widgets::EWT_RADIOBUTTON:
		main = GuiSM::Widgets::CSimpleRadioButton::Create(parentGui);
		main->setSize(rect);
		break;

        case GuiSM::Widgets::EWT_SCROLL:
		main = GuiSM::Widgets::CSimpleScroll::Create(rect, parentGui);
		break;

        case GuiSM::Widgets::EWT_TEXTBLOCK:
		//main = GuiSM::Widgets::CSimpleTextBlock::Create(rect, parent);
		break;

        case GuiSM::Widgets::EWT_WINDOW:
		main = GuiSM::Widgets::CWidget<EWT_WINDOW, EWA_RENDER | EWA_TEXTURE | EWA_TEXT>::Create(rect, parentGui );
		//main = GuiSM::Widgets::CSimpleWindow::Create(rect, parent );
		break;

        case GuiSM::Widgets::EWT_NONE:
		break;
					
	}
}

void EditableWidget::CreateHelpers(int size)
{
	move = CSimpleButton::Create(Rect(0, 0, size, size), main);
	int y = main->getSize().height;
	int x = main->getSize().width;
	resize = CSimpleButton::Create(Rect(x-size, y-size, size, size), main);
	handleSize = size;
}

void EditableWidget::Delete()
{
	
	int csize = children.size();
	for(int i = 0; i < csize; ++i)
		children[i]->Delete();
	
	children.clear();
	
	main->getParent()->removeChild(main);
/*
	fastdelegate::FastDelegate< void(Events::MouseEvent* ) > delegate;
	delegate.bind(this, &EditableWidget::OnClick);		
	main->deattach( delegate, Events::EET_MOUSECLICK);

	fastdelegate::FastDelegate< void(Events::MouseButtonEvent* ) > delegateButton;

	delegateButton.bind(this, &EditableWidget::OnMoveButtonDown);
	move->deattach( delegateButton, Events::EET_MOUSEBUTTONDOWN);

	delegateButton.bind(this, &EditableWidget::OnMoveButtonUp);
	move->deattach( delegateButton, Events::EET_MOUSEBUTTONUP);

	delegateButton.bind(this, &EditableWidget::OnResizeButtonDown);
	resize->deattach( delegateButton, Events::EET_MOUSEBUTTONDOWN);

	delegateButton.bind(this, &EditableWidget::OnResizeButtonUp);
	resize->deattach( delegateButton, Events::EET_MOUSEBUTTONUP);

	
	fastdelegate::FastDelegate< void(Events::MouseMoveEvent* ) > delegateMove;

	delegateMove.bind(this, &EditableWidget::OnMoveWidget);
	move->deattach( delegateMove, Events::EET_MOUSEMOVE);

	delegateMove.bind(this, &EditableWidget::OnResizeWidget);
	resize->deattach( delegateMove, Events::EET_MOUSEMOVE);
	*/
	
}

/**
 * Utils
 */
void EditableWidget::ResizeByRatio(double xRatio, double yRatio)
{
	int y_pre = main->getSize().height;
	int x_pre = main->getSize().width;
	Rect size = main->getSize();
	Point pos = main->getPos();

	
	double dw = size.width * xRatio;
	double dx = pos.x * xRatio;		
	double dh = size.height* yRatio;
	double dy = pos.y * yRatio;
	
	size.width	= (dw - std::floor(dw)) >= 0.5 ? std::ceil(dw) : std::floor(dw);
	pos.x		= (dx - std::floor(dx)) >= 0.5 ? std::ceil(dx) : std::floor(dx);
	size.height = (dh - std::floor(dh)) >= 0.5 ? std::ceil(dh) : std::floor(dh);
	pos.y		= (dy - std::floor(dy)) >= 0.5 ? std::ceil(dy) : std::floor(dy);	

	main->setSize(size);
	main->setPos(pos);
	int y = main->getSize().height;
	int x = main->getSize().width;
	resize->setPos(Point(x-handleSize, y-handleSize));

	data.x = pos.x;
	data.y = pos.y;
	data.h = size.height;
	data.w = size.width;
	if(model)
			model->OnActiveWidgetResize();

	double xratio = (double)x / (double)x_pre;
	double yratio = (double)y / (double)y_pre;
	if(resizeChildren)
	{
		for(int i = 0; i < children.size(); ++i)
		{
			EditableWidgetPtr pWidget = children[i];
			pWidget->ResizeByRatio(xratio, yratio);
		}
	}
}

void EditableWidget::AfterResize()
{
	double y_new = main->getSize().height;
	double x_new = main->getSize().width;

	double xRatio = x_new / x;
	double yRatio = y_new / y;
	if(resizeChildren)
	{
		for(int i = 0; i < children.size(); ++i)
		{
			EditableWidgetPtr pWidget = children[i];
			pWidget->ResizeByRatio(xRatio, yRatio);
		}
	}
}

void EditableWidget::Activate()
{
	active = true;
	resize->setVisibility(true);
	move->setVisibility(true);
}

void EditableWidget::Deactivate()
{
	active = false;
	resize->setVisibility(false);
	move->setVisibility(false);
	dragging = false;
	resizing = false;
}

void EditableWidget::ToXml(pugi::xml_node root)
{
	pugi::xml_node widget = root.append_child("widget");
	widget.append_attribute("name").set_value(data.name.c_str());
        widget.append_attribute("type").set_value((unsigned int)(data.type));
	widget.append_attribute("texture_path").set_value( Utility::makePathRelativeTo( QString::fromStdString(data.texturePath), model->GetActivePath()).c_str());
	pugi::xml_node position = widget.append_child("position");
	position.append_attribute("x").set_value(data.x);
	position.append_attribute("y").set_value(data.y);
	pugi::xml_node size = widget.append_child("size");
	size.append_attribute("w").set_value(data.w);
	size.append_attribute("h").set_value(data.h);
	pugi::xml_node font_size = widget.append_child("font_size");
	font_size.append_attribute("w");
	font_size.append_attribute("h");

	int csize = children.size();
	for(int i = 0; i < csize; ++i)
		children[i]->ToXml(widget);
}

void EditableWidget::AddChild(EditableWidgetPtr widget) 
{ 
	children.push_back(widget); 
}

/**
 * Setters
 */
void EditableWidget::SetPosX(int x)				
{ 
	data.x = x; main->setPosX(x);
}

void EditableWidget::SetPosY(int y)				
{ 
	data.y = y; main->setPosY(y);
}

void EditableWidget::SetWidth(int width)		
{ 
	data.w = width; main->setWidth(width); 
}

void EditableWidget::SetHeight(int height)		
{ 
	data.h = height; main->setHeight(height); 
}

void EditableWidget::SetTexture(QString path)	
{
	data.texturePath = path.toStdString();  main->setTexture(*GuiSM::TextureManager::get().getTexture(path.toStdString()));
}

void EditableWidget::SetName(QString name)		
{ 
	data.name = name.toStdString(); main->setName(name.toStdString()); 
}

void EditableWidget::SetParent(EditableWidgetPtr parent)
{
	this->parent = parent;
}

EditableWidgetPtr EditableWidget::GetParent() const
{
	return parent;
}

bool EditableWidget::GetResizeChildren() const
{
	return resizeChildren;
}

void EditableWidget::SetResizeChildren(bool resize)
{
	resizeChildren = resize;
}
	
void EditableWidget::SetDeepResizeChildren(bool resize)
{
	resizeChildren = resize;
	for(int i = 0; i < children.size(); ++i)
	{
		EditableWidgetPtr pWidget = children[i];
		pWidget->SetDeepResizeChildren(resize);
	}
}

